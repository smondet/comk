let () =
  let open Cmdliner in
  let version = Comk.Meta.version in
  let default_cmd =
    let doc = "A thing, a bit like dmenu." in
    let sdocs = Manpage.s_common_options in
    let exits = Term.default_exits in
    ( Term.(ret (const (fun _ -> `Help (`Pager, None)) $ pure ()))
    , Term.info "comk-examples" ~version ~doc ~sdocs ~exits ) in
  let font_size_term =
    let doc = "Set the text size." in
    Arg.(value & opt float 20. & info ["text-size"] ~docv:"SIZE" ~doc) in
  let xdg_open_cmd =
    let open Term in
    ( ( pure (fun geometry text_size from ->
            Comk.Display.with_sdl ~geometry ~text_size
            @@ Comk.Interspec.Example.xdg_open ~from () ;
            () )
      $ Comk.Display.Geometry_options.cmdliner_term ()
      $ font_size_term
      $ Arg.(
          value
          & pos 0 string (Sys.getenv "HOME")
          & info [] ~doc:"The path to start with") )
    , Term.info "open" ~doc:"The File explorer" ) in
  let test_cmd =
    let open Term in
    ( pure (fun geometry text_size ->
          Comk.Display.with_sdl ~geometry ~text_size
            Comk.Interspec.Example.dbg_interactor )
      $ Comk.Display.Geometry_options.cmdliner_term ()
      $ font_size_term
    , Term.info "test" ~doc:"The some random test (uses `xmessage`)" ) in
  let test_multi_cmd =
    let open Term in
    let term =
      const (fun geometry text_size attempts ->
          for attempt = 1 to attempts do
            Comk.Common.dbg "Attempt: %d" attempt ;
            Comk.Display.with_sdl ~geometry ~text_size
              Comk.Interspec.Example.just_debug_printing
          done )
      $ Comk.Display.Geometry_options.cmdliner_term ()
      $ font_size_term
      $ Arg.(
          required
            (pos 0 (some int) None
               (info [] ~doc:"The Number of successive windows.") )) in
    (term, Term.info "multi-test" ~doc:"Do multiple-window tests.") in
  let cmds = [test_cmd; xdg_open_cmd; test_multi_cmd] in
  Term.(exit @@ eval_choice default_cmd cmds)
