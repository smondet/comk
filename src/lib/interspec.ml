open Common

type action_result = Done | Menu of t

and action = item list -> action_result

and item =
  { selection_key: char option
  ; matching_text: string option
  ; display_text: string
  ; action: action }

and t = {items: item list}

let item_to_text {selection_key; display_text; _} =
  sprintf "[%s] %s"
    (Option.value_map selection_key ~default:" " ~f:String.of_character)
    (* (Option.value matching_text ~default:"_") *)
    display_text

let text_to_display t = List.map t.items ~f:item_to_text
let make items = {items}

let item ?key ?matching display_text action =
  {selection_key= key; matching_text= matching; display_text; action}

let effect f _ = f () ; Done
let sub items _ = Menu {items}
let menu items = sub items ()

module Example = struct
  let xdg_open ?(from = Sys.getenv "HOME") () =
    let ( // ) = Filename.concat in
    let dir p = Sys.readdir p |> Array.to_list in
    (* let keys = *)
    (*   List.init 10 (fun i -> char_of_int (i + int_of_char '0')) *)
    (*   @ List.init 26 (fun i -> char_of_int (i + int_of_char 'a')) in *)
    let rec dir_item ?key p =
      let matching = Filename.basename p in
      item ?key ~matching matching (fun _ ->
          if Sys.is_directory p then sub (of_dir p) ()
          else effect (fun () -> System.detach ["xdg-open"; p]) () )
    and of_dir p = List.map (dir p) ~f:(fun s -> dir_item (p // s)) in
    make (of_dir from)

  let dbg_interactor =
    let sels c =
      List.map c ~f:(fun i -> i.display_text) |> String.concat ~sep:", "
      (* String.concat ~sep:" -> " (List.map c ~f:selection_to_string) *) in
    let xmsg s = System.detach ["xmessage"; "-center"; "-timeout"; "3"; s] in
    let xmk key s =
      item ~key s (fun chl ->
          ksprintf xmsg "<    %s [%s]    >" s (sels chl) ;
          Done ) in
    let xms s =
      item ~matching:s s (fun sl ->
          ksprintf xmsg "<    %s [%s]    >" s (sels sl) ;
          Done ) in
    make
      [ xmk '1' "The one"; xmk '2' "The two"
      ; item ~key:'x' "Browse $HOME" (fun _ -> Menu (xdg_open ()))
      ; item ~key:'w' "Sub-menu chars"
        @@ sub
             (List.init 5 ~f:(fun s ->
                  xmk (char_of_int (100 + s)) (sprintf "The %dth" s) ) )
      ; item ~key:'s' "Sub-menu strings"
          (["one"; "two"; "a bigger one"] |> List.map ~f:xms |> sub) ]

  let just_debug_printing =
    let rec items () =
      [ item ~key:'r' "Stay in menu" (fun x ->
            dbg "just_debug_printing: Staying ('r')" ;
            sub (items ()) x )
      ; item ~key:'q' "Quit" (fun _ ->
            effect (fun () -> dbg "just_debug_printing: Quitting ('q')\n%!") () )
      ] in
    make (items ())
end
