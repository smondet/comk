(** Create an SDL window and display menus from {!Interspec.t} values.  *)

(** Configure the geometry of the window with {!Geometry_options.t}
    values. *)
module Geometry_options : sig
  type t = {w: int; h: int; x: int option; y: int option}

  val default_no_position : t
  val default_top_left : t
  val default : t

  val cmdliner_term :
    ?default:t -> ?manpage_section:string -> unit -> t Cmdliner.Term.t
end

val with_sdl :
  ?text_size:float -> ?geometry:Geometry_options.t -> Interspec.t -> unit
(** [with_sdl] is the main function of the [Comk] library, it starts
    an SDL window, displays the menu, and deals with all the
    interaction. *)
