module String = Sosa.Native_string
module Option = Nonstd.Option
module List = Nonstd.List
module Array = Nonstd.Array
include Printf

module Float = struct
  include Nonstd.Float

  let ( + ) = ( +. )
  let ( - ) = ( -. )
  let ( * ) = ( *. )
  let ( / ) = ( /. )
  let pi = 4.0 * atan 1.
  let mid a b = (a + b) / 2.
end

let zero = Unix.gettimeofday ()

let dbg fmt =
  ksprintf
    (fun s ->
      let t = Unix.gettimeofday () -. zero in
      eprintf "COMKDBG[%f]: %s\n%!" t s)
    fmt

module System = struct
  let detach ?(log_to = ("/dev/null", "/dev/null")) = function
    | [] -> invalid_arg "detach"
    | exec :: _ as l -> (
      match Unix.fork () with
      | 0 -> (
          Unix.setsid () |> ignore ;
          match Unix.fork () with
          | 0 ->
              (* https://ocaml.github.io/ocamlunix/ocamlunix.html#sec108 *)
              let flags = [Unix.O_APPEND; Unix.O_WRONLY; Unix.O_CREAT] in
              let o = Unix.openfile (fst log_to) flags 0o600 in
              let e = Unix.openfile (snd log_to) flags 0o600 in
              Unix.dup2 o Unix.stdout ;
              Unix.dup2 e Unix.stderr ;
              Unix.close o ;
              Unix.close e ;
              Unix.execvp exec (Array.of_list l)
          | _ -> () )
      | _ -> () )

  let read_lines p =
    let o = open_in p in
    let r = ref [] in
    try
      while true do
        r := input_line o :: !r
      done ;
      assert false
    with _ -> close_in o ; List.rev !r

  let cmd_to_string_list cmd =
    let i = Unix.open_process_in cmd in
    let rec loop acc =
      try loop (input_line i :: acc) with _ -> close_in i ; List.rev acc in
    loop []

  let write_lines p l =
    let o = open_out p in
    List.iter l ~f:(fprintf o "%s\n") ;
    close_out o
end
