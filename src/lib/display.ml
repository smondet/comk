open Common

module Geometry_options = struct
  type t = {w: int; h: int; x: int option; y: int option}

  let default_no_position = {w= 900; h= 600; x= None; y= None}
  let default_top_left = {default_no_position with x= Some 0; y= Some 0}
  let default = default_no_position

  let cmdliner_term ?(default = default) ?(manpage_section = "GEOMETRY OPTIONS")
      () =
    let open Cmdliner in
    let make optname typ default =
      let doc = sprintf "Set the window %s." optname in
      let open Term in
      pure (fun v -> v)
      $ Arg.(
          value & opt typ default
          & info [optname] ~docs:manpage_section ~docv:"INT" ~doc) in
    let open Term in
    pure (fun w h x y -> {w; h; x; y})
    $ make "width" Arg.int default.w
    $ make "height" Arg.int default.h
    $ make "x-coordinate" Arg.(some int) default.x
    $ make "y-coordinate" Arg.(some int) default.y
end

module Draw = struct
  module WRend = Wall.Renderer
  module WImg = Wall.Image
  module WPath = Wall.Path
  module WPaint = Wall.Paint
  module WText = Wall_text
  module WFont = Wall_text.Font
  module WTransform = Wall.Transform
  module WOutline = Wall.Outline
  module WColor = Wall.Color

  let deja_vu_sans_mono_content =
    lazy
      (let length =
         List.fold_left
           ~f:(fun c s -> c + String.length s + 1)
           ~init:0 Deja_vu_sans_mono.ttf in
       let buffer =
         Bigarray.Array1.create Bigarray.int8_unsigned Bigarray.c_layout length
       in
       let index = ref 0 in
       List.iter Deja_vu_sans_mono.ttf ~f:(fun s ->
           String.iter s ~f:(fun c ->
               buffer.{!index} <- int_of_char c ;
               incr index ) ;
           buffer.{!index} <- int_of_char '\n' ;
           incr index ) ;
       let offset = List.hd_exn (Stb_truetype.enum buffer) in
       (buffer, offset) )

  let deja_vu_sans_mono () =
    let buffer, offset = Lazy.force deja_vu_sans_mono_content in
    match Stb_truetype.init buffer offset with
    | None -> assert false
    | Some font -> font

  module Mouse = struct
    type t = {x: float; y: float}

    let of_ints ix iy = {x= float ix; y= float iy}

    (* let to_string {x; y} = sprintf "(%.2f, %.2f)" x y *)

    let within_rectangle t ~x ~y ~w ~h =
      x <= t.x && t.x <= x +. w && y <= t.y && t.y <= y +. h
  end

  module Screen = struct
    type t = {w: float; h: float; margin: float}

    let of_ints ~margin iw ih = {w= float iw; h= float ih; margin}

    (* let v2 {w; h; _} = Gg.V2.v w h *)

    let to_string {w; h; margin} = sprintf "{%f×%f++%f}" w h margin
  end

  let draw_text paint font ~x ~y text =
    WImg.paint paint (WText.simple_text font ~x ~y text)

  let render ~mouse_font ~item_font ~w ~h ~mouse_x ~mouse_y ~text_size context
      (t : State.t) =
    let screen = Screen.of_ints ~margin:10. w h in
    let mouse = Mouse.of_ints mouse_x mouse_y in
    let items =
      let open Float in
      let texts =
        let to_display = (float h / text_size) - 5. |> truncate in
        (false, State.input_to_string t) :: State.to_display to_display t in
      let x = 10. in
      let y = 30. in
      let margin = 10. in
      List.mapi texts ~f:(fun ith (sel, msg) ->
          let y = y + text_size + (float ith * text_size) in
          let rect =
            let x = x + (margin / 2.) in
            let y = y - (0.8 * text_size) in
            let w = float w - 30. in
            let h = text_size + 2. in
            if Mouse.within_rectangle mouse ~x ~y ~w ~h then
              WImg.paint
                (WPaint.rgba 0.5 0. 0. 0.3)
                (WImg.fill_path
                   (* WOutline.{default with stroke_width= 1.} *)
                   (fun ctx -> WPath.round_rect ctx ~x ~y ~w ~h ~r:3.) )
            else
              WImg.paint (WPaint.rgba 0. 0. 0. 0.3)
                (WImg.stroke_path
                   WOutline.{default with stroke_width= 1.}
                   (fun ctx -> WPath.round_rect ctx ~x ~y ~w ~h ~r:3.) ) in
          WImg.seq
            [ rect
            ; draw_text
                ( if sel then WPaint.rgba 0.2 0.6 0.2 1.
                else WPaint.rgba 0.2 0.2 0.2 1. )
                item_font ~x:(x + margin) ~y msg ] ) in
    let wim =
      WImg.seq
        [ WImg.paint (WPaint.rgba 0. 0. 0. 0.5)
            (WImg.stroke_path
               WOutline.{default with stroke_width= 3.}
               (fun ctx ->
                 WPath.round_rect ctx ~x:10. ~y:10.
                   ~w:(float w -. 20.)
                   ~h:(float h -. 20.)
                   ~r:1. ) ); WImg.seq items
        ; draw_text (WPaint.rgba 0. 0. 0. 0.5) mouse_font ~x:mouse.Mouse.x
            ~y:mouse.Mouse.y (Screen.to_string screen) ] in
    WRend.render context ~width:(float w) ~height:(float h) wim
end

let with_sdl ?(text_size = 20.) ?(geometry = Geometry_options.default)
    interactor =
  let open Tsdl in
  let open Tgles2 in
  (* let open Tsdl_ttf in *)
  let open Rresult in
  dbg "Starting" ;
  R.ignore_error
    ~use:(function `Msg e -> failwith (Printf.sprintf "Error %s" e))
    ( Sdl.init Sdl.Init.(everything)
    >>= fun () ->
    Sdl.create_window_and_renderer
      Sdl.Window.(borderless + windowed + opengl + resizable)
      ~w:geometry.Geometry_options.w ~h:geometry.Geometry_options.h
    >>= fun (window, renderer) ->
    Sdl.render_set_logical_size renderer geometry.Geometry_options.w
      geometry.Geometry_options.h
    >>= fun () ->
    let xy =
      match (geometry.Geometry_options.x, geometry.Geometry_options.y) with
      | Some v, Some w -> Some (v, w)
      | Some v, None -> Some (v, 0)
      | None, Some w -> Some (0, w)
      | None, None -> None in
    Option.iter xy ~f:(fun (x, y) -> Sdl.set_window_position window ~x ~y) ;
    (* Sdl.set_window_title window window_title ; *)
    (* if disable_screen_saver then Sdl.disable_screen_saver () ; *)
    let e = Sdl.Event.create () in
    Sdl.gl_set_attribute Sdl.Gl.doublebuffer 1
    >>= fun () ->
    Sdl.gl_create_context window
    >>= fun gl_context ->
    Sdl.gl_make_current window gl_context
    >>= fun () ->
    let wall_context =
      Wall.Renderer.create ~antialias:true ~stencil_strokes:true () in
    let deja_vu = Draw.deja_vu_sans_mono () in
    let mouse_font = Wall_text.Font.make ~size:text_size deja_vu in
    let item_font =
      Wall_text.Font.make (* ~line_height:10. *) ~size:text_size deja_vu in
    let rec loop iteration state =
      if iteration <= 2 then dbg "loop %d" iteration ;
      if iteration >= 1 then Unix.sleepf 0.010 ;
      let w, h = Sdl.get_window_size window in
      Gl.viewport 0 0 w h ;
      Gl.clear_color 1. 1. 1. 0.5 ;
      Gl.(clear (color_buffer_bit lor depth_buffer_bit lor stencil_buffer_bit)) ;
      Gl.enable Gl.blend ;
      Gl.blend_func_separate Gl.one Gl.src_alpha Gl.one Gl.one_minus_src_alpha ;
      Gl.enable Gl.cull_face_enum ;
      Gl.disable Gl.depth_test ;
      let _, (mouse_x, mouse_y) = Sdl.get_mouse_state () in
      Draw.render ~mouse_font ~item_font ~w ~h ~mouse_x ~mouse_y ~text_size
        wall_context state ;
      Sdl.gl_swap_window window ;
      if iteration <= 2 then dbg "swapped win %d" iteration ;
      let rec process_events state =
        match Sdl.wait_event_timeout (Some e) 10 with
        | false ->
            `Loop state
            (* Sdl.delay 10l ; *)
            (* loop (iteration + 1) state *)
        | true -> (
            let key_scancode e =
              Sdl.Scancode.enum Sdl.Event.(get e keyboard_scancode) in
            let key_code e = Sdl.Event.(get e keyboard_keycode) in
            let ctrl = Sdl.get_mod_state () land Sdl.Kmod.ctrl <> 0 in
            let alt = Sdl.get_mod_state () land Sdl.Kmod.alt <> 0 in
            let event e = Sdl.Event.(enum (get e typ)) in
            let window_event e =
              Sdl.Event.(window_event_enum (get e window_event_id)) in
            let is v = key_scancode e = v in
            let is_alt v = is v && alt in
            let is_ctrl v = is v && ctrl in
            match event e with
            | `Quit -> `Quit window
            | `Key_up when is `Escape || is_ctrl `Q -> `Quit window
            | `Key_up when is `Down || is_alt `J ->
                process_events (State.next_selection state)
            | `Key_up when is `Up || is_alt `K ->
                process_events (State.previous_selection state)
            | `Key_up when is `Right || is `Return || is_alt `L -> (
              match State.do_selection state with
              | Some s -> process_events s
              | None -> `Quit window )
            | `Key_up when is `Backspace || is_alt `H ->
                process_events (State.pop_history state)
            | `Key_up when (not alt) && not ctrl -> (
              match State.on_key state (key_code e) with
              | Some new_state -> process_events new_state
              | None -> `Quit window )
            | `Window_event -> (
              match window_event e with
              | `Exposed | `Resized -> `Loop state
              | _ -> `Loop state )
            | _ -> `Loop state ) in
      match process_events state with
      | `Loop s -> loop (iteration + 1) s
      | `Quit w -> Ok w in
    loop 0 (State.make interactor)
    >>= fun window ->
    Sdl.destroy_window window ;
    (* Ttf.quit (); *)
    Sdl.gl_delete_context gl_context ;
    (* Sdl.quit () ; *)
    Ok () )
