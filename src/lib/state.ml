open Common

type input = Single_key | Text of (char list * string list)

type t =
  { root: Interspec.t
  ; current: Interspec.t
  ; visible: Interspec.item list
  ; input: input
  ; selection: int
  ; history: (input * Interspec.item * Interspec.t) list }

let current t = t.current

let to_display how_many t =
  let from = if t.selection < how_many then 0 else t.selection - how_many + 1 in
  let rec loop idx count acc = function
    | [] -> List.rev acc
    | _ when count >= how_many -> List.rev acc
    | _ :: b when idx < from -> loop (idx + 1) count acc b
    | a :: b ->
        loop (idx + 1) (count + 1)
          ((idx = t.selection, Interspec.item_to_text a) :: acc)
          b in
  loop 0 0 [] t.visible

let fresh_input_state interact =
  let open Interspec in
  if
    List.exists interact.items ~f:(function
      | {selection_key= Some _; _} -> true
      | {selection_key= None; _} -> false)
  then Single_key
  else Text ([], [])

let make i =
  { root= i
  ; current= i
  ; selection= 0
  ; visible= i.Interspec.items
  ; input= fresh_input_state i
  ; history= [] }

let change_current t (how, item) current =
  { t with
    current
  ; selection= 0
  ; visible= current.items
  ; history= (how, item, t.current) :: t.history
  ; input= fresh_input_state current }

let update_matches t new_text prev_text =
  (* Order of the strings does not count: *)
  let sl = String.of_character_list (List.rev new_text) :: prev_text in
  let visible =
    List.filter t.current.items ~f:(fun item ->
        match Option.map ~f:StringLabels.lowercase_ascii item.matching_text with
        | None -> false
        | Some t ->
            List.for_all sl ~f:(fun sub ->
                String.index_of_string t ~sub <> None)) in
  let selection =
    let lg = List.length visible in
    if t.selection < lg then t.selection else lg - 1 in
  {t with input= Text (new_text, prev_text); selection; visible}

let pop_history t =
  match t.input with
  | Single_key -> (
    match t.history with
    | [] ->
        { t with
          current= t.root
        ; visible= t.root.items
        ; input= fresh_input_state t.root }
    | (inp, _, one) :: more -> {t with current= one; history= more; input= inp}
    )
  | Text ([], []) -> {t with input= Single_key}
  | Text (_ :: more, strings) -> update_matches t more strings
  | Text ([], one :: more) ->
      update_matches t (String.to_character_list one) more

(* | Text (chars, strings) -> t *)

let input_to_string t =
  match t.input with
  | Single_key -> "[?]"
  | Text (chl, sl) ->
      sprintf "> [%s] %s|"
        (String.concat sl ~sep:";")
        (List.rev_map chl ~f:(sprintf "%c") |> String.concat ~sep:"")

let next_selection t =
  {t with selection= (t.selection + 1) mod List.length t.visible}

let previous_selection t =
  { t with
    selection=
      ( if t.selection = 0 then List.length t.visible - 1
      else (t.selection - 1) mod List.length t.visible ) }

let do_selection t =
  let open Interspec in
  match List.nth t.visible t.selection with
  | Some j -> (
    match
      j.action (j :: List.map ~f:(fun (_, i, _) -> i) t.history |> List.rev)
    with
    | Done -> None
    | Menu int -> Some (change_current t (t.input, j) int) )
  | None -> dbg "BUGGGG" ; None

let on_key t k =
  (* let as_char = (try char_of_int k with _ -> 'E') in *)
  let open Interspec in
  match (char_of_int k, t.input) with
  | exception _ ->
      dbg "Unintersting key: %d" k ;
      Some t
  | ' ', Single_key -> Some (update_matches t [] [])
  | ch, Single_key -> (
    match
      List.find t.visible ~f:(function
        | {selection_key; _} when selection_key = Some ch -> true
        | _ -> false)
    with
    | Some found -> (
      match
        found.action
          (found :: List.map ~f:(fun (_, i, _) -> i) t.history |> List.rev)
      with
      | Done -> None
      | Menu int -> Some (change_current t (t.input, found) int) )
    | None ->
        dbg "Unintersting key: %c, %d" ch k ;
        Some t )
  | ' ', Text (chl, prev) ->
      Some
        (update_matches t []
           (prev @ [String.of_character_list @@ List.rev chl]))
  | ch, Text (chl, prev) -> Some (update_matches t (ch :: chl) prev)

(* delijed *)
