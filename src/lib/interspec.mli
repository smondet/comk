type action_result = Done | Menu of t

and action = item list -> action_result

and item =
  { selection_key: char option
  ; matching_text: string option
  ; display_text: string
  ; action: action }

and t = {items: item list}

val make : item list -> t
val item : ?key:char -> ?matching:string -> string -> action -> item
val effect : (unit -> unit) -> 'a -> action_result
val sub : item list -> action
val menu : item list -> action_result
val item_to_text : item -> string
val text_to_display : t -> string list

module Example : sig
  val xdg_open : ?from:string -> unit -> t
  val dbg_interactor : t
  val just_debug_printing : t
end
