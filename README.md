Comk: Configurable Menus for the Keyboard
=========================================

Comk is a library allowing to create mini-graphical-apps a bit like the famous
[`dmenu`](https://tools.suckless.org/dmenu/) but it is more flexible.

See this blog [post](https://seb.mondet.org/b/0001-comk-intro.html): **Comk:
Graphical Menus In OCaml** (Sat, 20 Jun 2020).

It has to main *interaction modes*:

1. **Signle-Key-Mode**: when at least one of the menu items is associated with
   a character (argument `?key` of `Comk.Interspec.item`), the menu starts in
   single-keystroke mode. In this mode actions associated with menu items are
   activated just by hitting their given key. The `<Space>` is special, it
   allows to change to *string-matching-mode*.
2. **String-Matching-Mode**: this mode is similar to `dmenu` (`CtrlP` in Vim,
   `ivy`/`helm` in Emacs); menu items are matched against the characters typed
   (ignoring case). The `<Space>` baris used to separate the strings, the
   menu-items are matched against the *set* of strings (ignoring order).

There are a few key-bindings:

- `Enter`/`Alt-L`: activate the currently highlighted menu-item.
- `Down`/`Up` or `Alt-J`/`Alt-K`: move the highlighted selection.
- `Backspace`/`Alt-H`: “go back” i.e. delete current character if in
  *string-matching-mode* or go back in history otherwise.
- `Esc`/`Ctrl-Q`: quit the application.

Mouse support is work-in-progress …




Build
-----

    opam pin add comk . -kgit
    
or locally, with the examples:

    dune build @install

To get a valid opam repository:

    opam switch create . 4.12.0
    opam pin add -ny comk .
    opam install --deps-only comk
    opam install ocamlformat.0.19.0 merlin


Try The Examples
----------------

See

    dune exec src/examples/main.exe -- --help=plain

for instance

    dune exec src/examples/main.exe open .

is an explorer of the current directory (does not use single-key mode). You can
try all the features with:

    dune exec src/examples/main.exe test
    
(this uses `xmessage` for the “leaf” actions).

